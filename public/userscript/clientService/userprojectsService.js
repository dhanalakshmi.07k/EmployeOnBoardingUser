/**
 * Created by Pro on 8/9/2017.
 */
userapp.factory("userprojectsService", function ($http) {
    var formConfigObj={};
    var userprojectsCount;

    var getuserprojectsJsonConfig = function(){

        return $http.get('/userprojectsJsonConfig');
    }
    var setuserprojectsFromConfig=function(formConfig){
        formConfigObj=formConfig

    }
    var getuserprojectsFromConfig=function(){
        return formConfigObj
    }

    var saveuserprojectsDetails = function(userprojectsData){
        return $http.post('/userprojectsDetails',userprojectsData);
    }
    var getuserprojectsDetailsByRange=function(start,range){
        return $http.get('/userprojectsDetails/'+start+'/'+range);
    }
    var getuserprojectsDetailsCount = function(){
        return $http.get('/userprojectsDetails/count');
    }
    var getuserprojectsCount = function(){
        return userprojectsCount;
    }
    var setuserprojectsCount = function(val){
        userprojectsCount=val;
    }
    var updateuserprojectsDetails = function(companyDetails){
        return $http.post('/userprojectsDetails/update',companyDetails)
    }
    var deleteuserprojectsDetails = function(id){
        return $http.delete('/userprojectsDetails/'+id);
    }
    var getuserprojectsDetailsById = function(id){
        return $http.get('/userprojectsDetails/'+id);
    }

    var getuserprojectsDetailsByName = function(userprojectsName){
        return $http.post('/userprojectsDetails/name',userprojectsName);
    }

    var getAlluserprojectsName = function(){
        return $http.get('/userprojectsDetailsName');
    }

var getUserProjectByEmployeeId=function(employeeId){
return $http.get('/myProject/'+employeeId);
}

var lastmeetupByEmployeeid=function(employeeid){
return  $http.get('/myProjectlastmeetup/'+employeeid);
}



    return{
        getuserprojectsJsonConfig:getuserprojectsJsonConfig,
        setuserprojectsFromConfig:setuserprojectsFromConfig,
        getuserprojectsFromConfig:getuserprojectsFromConfig,
        saveuserprojectsDetails:saveuserprojectsDetails,
        getuserprojectsDetailsByRange:getuserprojectsDetailsByRange,
        getuserprojectsDetailsCount:getuserprojectsDetailsCount,
        getuserprojectsCount:getuserprojectsCount,
        setuserprojectsCount:setuserprojectsCount,
        updateuserprojectsDetails:updateuserprojectsDetails,
        deleteuserprojectsDetails:deleteuserprojectsDetails,
        getuserprojectsDetailsById:getuserprojectsDetailsById,
        getuserprojectsDetailsByName:getuserprojectsDetailsByName,
        getAlluserprojectsName:getAlluserprojectsName,
        getUserProjectByEmployeeId:getUserProjectByEmployeeId,
        lastmeetupByEmployeeid:lastmeetupByEmployeeid
    }






})
